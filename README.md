# NoCSS

Semantic markup should look good by default. NoCSS provides an option that is somewhere between a CSS reset and a UI framework.

[demo](https://organic-web.gitlab.io/no-css/)