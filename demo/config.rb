# Build-specific config (ie, for Gitlab pages)
configure :build do
  # Gitlab hosts in a subfolder by default, need to use relative assets/links
  activate :relative_assets
  set :relative_links, true
end
